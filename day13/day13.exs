#!/usr/bin/env elixir

defmodule Day13 do
  @filename "input.txt"

  def parseLine(line) do
    [ depth, range ] =
      String.trim(line)
        |> String.split(": ")
        |> Enum.map(&String.to_integer/1)
    { depth, range }
  end

  def parseFile(filename) do
    rangeMap =
      File.stream!(filename)
        |> Stream.map(&parseLine/1)
        |> Enum.reduce(%{ }, fn { depth, range }, map ->
              Map.put(map, depth, range)
            end)

    maxDepth = Map.keys(rangeMap) |> Enum.max

    for depth <- 0 .. maxDepth do
      Map.get(rangeMap, depth, 0)
    end
  end

  def position(0, _), do: nil
  def position(range, t) do
    period = 2 * range - 2
    p = rem(t, period)
    if p < range do
      p
    else
      period - p
    end
  end

  def isCollision(range, depth, delay) do
    position(range, depth + delay) == 0
  end

  def hasCollision(ranges, delay) do
    Stream.with_index(ranges)
      |> Stream.map(fn { range, depth } -> isCollision(range, depth, delay) end)
      |> Enum.any?
  end

  def severity(range, depth) do
    if isCollision(range, depth, 0) do
      depth * range
    else
      0
    end
  end

  def totalSeverity(ranges) do
    Enum.with_index(ranges)
      |> Enum.map(fn { range, depth } -> severity(range, depth) end)
      |> Enum.sum
  end

  def part1 do
    parseFile(@filename)
      |> totalSeverity
  end

  def search(ranges, offset, step) do
    Stream.iterate(offset, &(&1 + step))
      |> Stream.reject(fn delay -> hasCollision(ranges, delay) end)
      |> Enum.take(1)
      |> List.first
  end

  def part2 do
    ranges = parseFile(@filename)

    parent = self()
    childCount = 8
    for offset <- 0..(childCount-1),
      do: spawn(fn ->
        answer = search(ranges, offset, childCount)
        send(parent, answer)
      end)

    receive do
      answer -> IO.puts(answer)
    end
  end

end

IO.puts(Day13.part1())
IO.puts(Day13.part2())
