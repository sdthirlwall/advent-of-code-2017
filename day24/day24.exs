#!/usr/bin/env elixir

defmodule Day24 do

  def parseFile(filename) do
    File.stream!(filename)
      |> Stream.map(&parseLine/1)
      |> Enum.to_list
  end

  def parseLine(line) do
    [ lhs, rhs ] =
      String.trim(line)
        |> String.split("/")
        |> Enum.map(&String.to_integer/1)
    { lhs, rhs }
  end

  def validPiece({ lhs, rhs }, port) do
    lhs == port || rhs == port
  end

  def nextPort({ port, other }, port), do: other
  def nextPort({ other, port }, port), do: other

  def pieceStrength({ lhs, rhs }) do
    lhs + rhs
  end

  def makeBridges(strength, port, pieces) do
    strengths =
      for piece <- pieces,
          validPiece(piece, port),
          rest = Enum.reject(pieces, &(&1 == piece)),
          do: makeBridges(strength + pieceStrength(piece), nextPort(piece, port), rest)

    if Enum.empty?(strengths) do
      strength
    else
      Enum.max(strengths)
    end
  end

  def makeBridges(length, strength, port, pieces) do
    lengthStrengths =
      for piece <- pieces,
          validPiece(piece, port),
          rest = Enum.reject(pieces, &(&1 == piece)),
          do: makeBridges(length + 1, strength + pieceStrength(piece), nextPort(piece, port), rest)

    if Enum.empty?(lengthStrengths) do
      { length, strength }
    else
      Enum.max_by(lengthStrengths, fn { length, strength } ->
        length * 1_000_000 + strength
      end)
    end
  end

  def part1 do
    #{ filename } = { "test.txt" }
    { filename } = { "input.txt" }
    pieces = parseFile(filename)
    makeBridges(0, 0, pieces)
  end

  def part2 do
    #{ filename } = { "test.txt" }
    { filename } = { "input.txt" }
    pieces = parseFile(filename)
    { _, strength } = makeBridges(0, 0, 0, pieces)
    strength
  end

end

Day24.part1 |> IO.puts
Day24.part2 |> IO.puts
