#!/usr/bin/env elixir

defmodule Day15 do

  use Bitwise, only_operators: true

  def makeGenerator(factor, seed) do
    next = fn x -> rem(factor * x, 2147483647) end
    Stream.iterate(next.(seed), next)
  end

  def isMultipleOf(n) do
    fn x -> rem(x, n) == 0 end
  end

  def low16Matches({ lhs, rhs }) do
    (lhs &&& 0xffff) == (rhs &&& 0xffff)
  end

  def part1 do
    genA = makeGenerator(16807, 512)
    genB = makeGenerator(48271, 191)

    Stream.zip(genA, genB)
      |> Stream.take(40_000_000)
      |> Enum.count(&low16Matches/1)
  end

  def part2 do
    genA = makeGenerator(16807, 512) |> Stream.filter(isMultipleOf(4))
    genB = makeGenerator(48271, 191) |> Stream.filter(isMultipleOf(8))

    Stream.zip(genA, genB)
      |> Stream.take(5_000_000)
      |> Enum.count(&low16Matches/1)
  end

end

IO.puts(Day15.part1())
IO.puts(Day15.part2())
