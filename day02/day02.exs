#!/usr/bn/env elixir

defmodule Day2 do

  @filename "input.txt"

  def parseLine(line) do
    String.split(line, "\t")
      |> Enum.map(&String.to_integer/1)
  end

  def parseFile(filename) do
    File.stream!(filename)
      |> Stream.map(&String.trim_trailing/1)
      |> Enum.map(&parseLine/1)
  end

  def part1 do
    parseFile(@filename)
      |> Enum.map(fn row -> Enum.max(row) - Enum.min(row) end)
      |> Enum.sum
  end

  defp pairs([]), do: []

  defp pairs([a | as]) do
    for b <- as do
      {a, b}
    end ++ pairs(as)
  end

  def findDivisiblePair(row) do
    Enum.sort(row)
      |> pairs
      |> Enum.find(fn {a,b} -> rem(b, a) == 0 end)
  end

  def part2 do
    parseFile(@filename)
      |> Enum.map(&findDivisiblePair/1)
      |> Enum.map(fn {a, b} -> div(b, a) end)
      |> Enum.sum
  end
end

IO.puts(Day2.part1)
IO.puts(Day2.part2)
