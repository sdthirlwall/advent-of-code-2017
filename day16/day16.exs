#!/usr/bin/env elixir

defmodule StreamCycle do

  defp uniquePrefix(stream) do
    Stream.transform(stream, MapSet.new, fn
      _, :halt -> { :halt, nil }
      item, seen -> if MapSet.member?(seen, item) do
        { [ item ], :halt }
      else
        { [ item ], MapSet.put(seen, item) }
      end
    end)
  end

  # Returns { startIndex, cycleLength }
  def findCycle(stream) do
    unique = uniquePrefix(stream) # |> Enum.map(&Enum.join/1) |> IO.inspect
    repeatedItem = Enum.take(unique, -1) |> List.first
    loopStartIndex = Enum.find_index(unique, &(&1 == repeatedItem))
    loopLength = Enum.count(unique) - loopStartIndex - 1

    loopItems = Enum.drop(unique, -1) |> Enum.drop(loopStartIndex)

    { loopStartIndex, loopLength, loopItems }
  end

end

defmodule Day16 do

  def loadFile(filename) do
    {:ok, data} = File.open(filename, [:read], fn file ->
       IO.read(file, :all)
    end)
    String.trim(data)
  end

  def parseFile(data) do
    String.split(data, ",")
      |> Enum.map(&parseInst/1)
  end

  def parseInst("s" <> count) do
    { :spin, String.to_integer(count) }
  end

  def parseInst("x" <> args) do
    [ lhs, rhs ] = String.split(args, "/") |> Enum.map(&String.to_integer/1)
    { :exchange, min(lhs, rhs), max(lhs, rhs) }
  end

  def parseInst("p" <> args) do
    [ lhs, rhs ] = String.split(args, "/")
    { :partner, lhs, rhs }
  end

  def swap(programs, lhs, rhs) do
    Enum.map(programs, fn
      ^lhs -> rhs
      ^rhs -> lhs
      any  -> any
    end)
  end

  def doInstr({ :spin, count }, programs) do
    { lhs, rhs } = Enum.split(programs, -count)
    rhs ++ lhs
  end

  def doInstr({ :exchange, lhs, rhs }, programs) do
    swap(programs, Enum.at(programs, lhs), Enum.at(programs, rhs))
  end

  def doInstr({ :partner, lhs, rhs }, programs) do
    swap(programs, lhs, rhs)
  end

  def dance(moves, programs) do
    Enum.reduce(moves, programs, &doInstr/2)
  end

  def dancen(moves, programs, n) do
    Stream.iterate(programs, &(dance(moves, &1)))
      |> Stream.drop(n)
      |> Enum.take(1)
      |> List.first
  end

  def part1 do
    programs = String.graphemes("abcdefghijklmnop")
    loadFile("input.txt")
      |> parseFile
      |> dance(programs)
      |> Enum.join
  end

  def part2 do
    moves = loadFile("input.txt") |> parseFile
    { loopStart, loopSize, loopItems } =
      String.graphemes("abcdefghijklmnop")
        |> Stream.iterate(&(dance(moves, &1)))
        |> StreamCycle.findCycle

    index = rem(1_000_000_000 - loopStart, loopSize)
    Enum.at(loopItems, index)
      |> Enum.join
  end
end

IO.puts(Day16.part1())
IO.puts(Day16.part2())
