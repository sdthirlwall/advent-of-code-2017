#!/usr/bin/env elixir

defmodule Day23 do

  defmodule CPU do
    defstruct inst: [ ], reg: %{ }, ip: 0, running: true, muls: 0
  end

  def fetch(cpu, arg) do
    case Integer.parse(arg) do
      { imm, "" } -> imm
      :error -> Map.get(cpu.reg, arg, 0)
    end
  end

  def store(cpu, reg, value) do
    %{cpu | reg: Map.put(cpu.reg, reg, value) }
  end

  def parseInst([ "jnz", src, offset ]) do
    fn cpu ->
      if fetch(cpu, src) != 0 do
        %{cpu | ip: cpu.ip + fetch(cpu, offset) - 1}
      else
        cpu
      end
    end
  end

  def parseInst([ "mul", dest, src ]) do
    fn cpu ->
      cpu = store(cpu, dest, fetch(cpu, dest) * fetch(cpu, src))
      %{ cpu | muls: cpu.muls + 1 }
    end
  end

  def parseInst([ "set", dest, src ]) do
    fn cpu ->
      store(cpu, dest, fetch(cpu, src))
    end
  end

  def parseInst([ "sub", dest, src ]) do
    fn cpu ->
      store(cpu, dest, fetch(cpu, dest) - fetch(cpu, src))
    end
  end

  def parseInst(_unmatched) do
    fn cpu ->
      %{cpu | running: false }
    end
  end

  def makeCPU(inst) do
    %CPU{ inst: inst }
  end

  def step(cpu) do
    case Enum.at(cpu.inst, cpu.ip) do
      nil  -> %{cpu | running: false}
      inst -> inst.(%{cpu | ip: cpu.ip + 1 })
    end
  end

  def run(cpu) do
    if cpu.running do
      step(cpu) |> run
    else
      cpu
    end
  end

  def parseLine(line) do
    String.trim(line)
      |> String.split(~r/\s+/)
      |> parseInst
  end

  def parseFile(filename) do
    File.stream!(filename)
      |> Stream.map(&parseLine/1)
      |> Enum.to_list
      |> makeCPU
  end

  def part1 do
    parseFile("input.txt")
      |> run
      |> Map.get(:muls)
  end

  def isPrime(n) do
    lowPrimes = [ 2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37 ]
    { _, isFactor } =
      Stream.concat(lowPrimes, Stream.iterate(39, &(&1 + 2)))
        |> Stream.map(fn f -> { f, rem(n, f) == 0 } end)
        |> Enum.find(fn { f, isFactor } -> isFactor || f * f > n end)

    !isFactor
  end

  def range(lo, hi, step) do
    Stream.iterate(lo, &(&1 + step))
      |> Stream.take_while(&(&1 <= hi))
  end

  def part2 do
    range(106500, 106500 + 17000, 17)
      |> Stream.reject(&isPrime/1)
      |> Enum.count
  end

end

Day23.part1 |> IO.puts
Day23.part2 |> IO.puts
