#!/usr/bin/env elixir

defmodule Day7 do

  @filename "input.txt"

  def parseLine(line) do
    regex = ~r/^(?<name>\S+)\s+\((?<weight>\d+)\)(?:\s+->\s+(?<rest>.*))?/
    match = Regex.named_captures(regex, line)
    children = case match["rest"] do
      ""    -> []
      names -> String.split(names, ", ")
    end
    %{
      :name     => match["name"],
      :weight   => String.to_integer(match["weight"]),
      :children => children,
    }
  end

  def parseFile(filename) do
    File.stream!(filename)
      |> Stream.map(&parseLine/1)
  end

  def findRoot(nodeList) do
    parentOf = Enum.reduce(nodeList, %{}, fn(node, map) ->
        Enum.reduce(node.children, map, fn(name, map) ->
          Map.put(map, name, node.name)
        end)
      end)

    Enum.find(nodeList, &(!Map.has_key?(parentOf, &1.name)))
  end

  def buildTreeRec(root, nodeMap) do
    Map.update!(root, :children, fn(childNames) ->
        Enum.map(childNames, fn(childName) ->
          Map.get(nodeMap, childName) |> buildTreeRec(nodeMap)
        end)
      end)
  end

  def buildTree(nodeList) do
    nodeMap = Enum.reduce(nodeList, %{}, fn(node, map) ->
        Map.put(map, node.name, node)
      end)

    root = findRoot(nodeList)

    buildTreeRec(root, nodeMap)
  end

  # Recursively applies f to the child nodes, then applies f to node.
  def walkTree(node, f) do
    node = Map.update!(node, :children, fn(children) ->
        Enum.map(children, &(walkTree(&1, f)))
      end)

    f.(node)
  end

  def assignTotalWeight(root) do
    getTotalWeight = fn(node) ->
        Enum.reduce(node.children, node.weight, &(&1.totalWeight + &2))
      end

    walkTree(root, fn(node) ->
        Map.put(node, :totalWeight, getTotalWeight.(node))
      end)
  end

  def isBalanced?(%{ children: [ ] }), do: false
  def isBalanced?(%{ children: [ c | cs ] }) do
    Enum.all?(cs, &(&1.totalWeight === c.totalWeight))
  end

  def findDeepestUnbalanced(node) do
    case Enum.find(node.children, &(!isBalanced?(&1))) do
      nil -> node
      unbalancedNode -> findDeepestUnbalanced(unbalancedNode)
    end
  end

  def adjustUnbalanced(node) do
    # All nodes have the same totalWeight but one.
    # If we sort them and the first != second, it's the first, otherwise its
    # the last.
    sorted = Enum.sort_by(node.children, &(&1.totalWeight))
    [ a, b | _ ] = sorted

    if (a.totalWeight < b.totalWeight) do
      # The first one is too low, adjust it up
      a.weight + b.totalWeight - a.totalWeight
    else
      # The last one is too high, adjust it down
      [ a, b | _ ] = Enum.reverse(sorted)
      a.weight + b.totalWeight - a.totalWeight
    end
  end

  def part1 do
    parseFile(@filename)
      |> buildTree
      |> Map.get(:name)
  end

  def part2 do
    parseFile(@filename)
      |> buildTree
      |> assignTotalWeight
      |> findDeepestUnbalanced
      |> adjustUnbalanced
  end
end

IO.puts(Day7.part1)
IO.puts(Day7.part2)
