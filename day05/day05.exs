#!/usr/bin/env elixir

defmodule Day5 do

  @filename "input.txt"

  def parseLine(line) do
    String.to_integer(line)
  end

  def parseFile(filename) do
    File.stream!(filename)
      |> Stream.map(&String.trim_trailing/1)
      |> Stream.map(&parseLine/1)
      |> Stream.with_index(0)
      |> Enum.reduce(%{ }, fn { x, i }, map -> Map.put(map, i, x) end)
  end

  def solve(increment) do
    update = fn x ->
      { x, increment.(x) }
    end

    doMove = fn {index, maze} ->
      { jump, maze } = Map.get_and_update(maze, index, update)
      { index + jump, maze }
    end

    maze = parseFile(@filename)
    bounds = 0 .. Map.size(maze) - 1

    Stream.iterate({ 0, maze }, doMove)
      |> Stream.with_index()
      |> Enum.find(fn {{pos, _maze}, _moves} -> ! pos in bounds end)
      |> elem(1)
  end

  def part1 do
    solve(fn x -> x + 1 end)
  end

  def part2 do
    solve(fn x -> if x >= 3 do x - 1 else x + 1 end end)
  end
end

IO.puts(Day5.part1)
IO.puts(Day5.part2)
