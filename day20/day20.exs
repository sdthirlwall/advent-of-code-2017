#!/usr/bin/env elixir

defmodule Day20 do

  defmodule Vector do
    defstruct [ :x, :y, :z ]

    def parse(str) do
      v = Regex.named_captures(~r/^(?<name>[pva])=<(?<x>-?\d+),(?<y>-?\d+),(?<z>-?\d+)>$/, str)
      { v["name"], %Vector{ x: String.to_integer(v["x"]),
                            y: String.to_integer(v["y"]),
                            z: String.to_integer(v["z"]) } }
    end

    def manhattanDistance(v) do
      abs(v.x) + abs(v.y) + abs(v.z)
    end

    def add(v0, v1) do
      %Vector{ x: v0.x + v1.x, y: v0.y + v1.y, z: v0.z + v1.z } end

    def scale(v, s) do
      %Vector{ x: v.x * s, y: v.y * s, z: v.z * s }
    end

    def sub(v0, v1) do
      add(v0, scale(v1, -1))
    end
  end

  defmodule Particle do
    defstruct [ :p, :v, :a, :index ]

    def make([ { _p, p }, { _v, v }, { _a, a } ], index) do
      %Particle{ p: p, v: v, a: a, index: index }
    end

    def update(p) do
      vel = Vector.add(p.v, p.a)
      pos = Vector.add(p.p, vel)
      %{p | p: pos, v: vel }
    end
  end

  def parseLine({ line, index }) do
    String.trim(line)
      |> String.split(", ")
      |> Enum.map(&Vector.parse/1)
      |> Particle.make(index)
  end

  def parseFile(filename) do
    File.stream!(filename)
      |> Stream.with_index
      |> Stream.map(&parseLine/1)
  end

  def part1 do
    parseFile("input.txt")
      |> Enum.min_by(fn p -> Vector.manhattanDistance(p.a) end)
      |> Map.get(:index)
  end

  def removeCollisions(particles) do
#    particles
#      |> Enum.group_by(fn p -> p.p end)
#      |> Map.values
#      |> Enum.filter(fn list -> tl(list) != [] end)
#      |> IO.inspect

    particles
      |> Enum.group_by(fn p -> p.p end)
      |> Map.values
      |> Enum.filter(fn list -> tl(list) == [] end)
      |> List.flatten
  end

  def step(particles) do
    Enum.map(particles, &Particle.update/1)
      |> removeCollisions
  end

  def parametric(p) do
    # a/2 t^2 + (v + a/2)t + p
    halfa = Vector.scale(p.a, 0.5)
    { halfa, Vector.add(p.v, halfa), p.p }
  end

  def solve({ a = %Vector{}, b = %Vector{}, c = %Vector{} }) do
    sx = solve({ a.x, b.x, c.x })
    sy = solve({ a.y, b.y, c.y })
    sz = solve({ a.z, b.z, c.z })
    #IO.inspect({ sx, sy, sz })

    MapSet.intersection(sx, sy) |> MapSet.intersection(sz)
  end

  def solve(x = { 0.0, 0.0, c }) do
    MapSet.new
  end

  def solve(x = { 0.0, b, c }) do
    #IO.inspect(x)
    MapSet.new([ -c / b ])
  end

  def solve(x = { a, b, c }) do
    #IO.inspect(x)
    det = b * b - 4 * a * c
    if (det < 0) do
      MapSet.new
    else
      sqrtDet = :math.sqrt(det)
      MapSet.new([ (-b + sqrtDet) / (2 * a), (-b - sqrtDet) / (2 * a) ])
    end
  end

  def intersect(pa, pb) do
    { a0, b0, c0 } = parametric(pa)
    { a1, b1, c1 } = parametric(pb)

    c = ({ Vector.sub(a1, a0), Vector.sub(b1, b0), Vector.sub(c1, c0) })
    #IO.inspect(c)
    solve(c) |> MapSet.to_list |> Enum.sort |> List.first
  end

  def findCollisions(particles) do
    collisions =
      for lhs <- particles,
          rhs <- particles,
          lhs.index < rhs.index,
          t = intersect(lhs, rhs),
          t !== nil,
          do: { t, [ lhs, rhs ] }

    if Enum.empty?(collisions) do
      MapSet.new
    else
      groups = Enum.group_by(collisions, fn { t, _ } -> t end)
      firstTime = Map.keys(groups) |> Enum.min
      groups[firstTime]
        |> Enum.flat_map(fn { _, particles } -> particles end)
        |> MapSet.new
    end
  end

  def iterateCollisions(particles) do
    IO.puts("#{ MapSet.size(particles) } particles remain")
    collisions = findCollisions(particles)

    if Enum.empty?(collisions) do
      MapSet.size(particles)
    else
      MapSet.difference(particles, collisions)
        |> iterateCollisions
    end
  end

  def part2 do
    parseFile("input.txt")
      |> Stream.iterate(&step/1)
      |> Stream.map(fn list -> Enum.count(list) end)
      |> Stream.dedup
      |> Stream.map(&IO.inspect/1)
      |> Stream.run
  end

  def part3 do
    parseFile("input.txt")
      |> MapSet.new
      |> iterateCollisions
      |> IO.inspect
  end

end

#Day20.part1 |> IO.puts
#Day20.part2 |> IO.puts
Day20.part3
