#!/usr/bin/env elixir

defmodule Day9 do
  @filename "input.txt"

  def loadFile(filename) do
    {:ok, data} = File.open(filename, [:read], fn file ->
      IO.read(file, :all)
    end)
    String.trim(data)
  end

  def parseGroup(stream, score, count, depth) do
    case stream do
      ["!", _ | cs] -> parseGroup(cs, score, count, depth)  # skip escaped chars
      ["," | cs ]   -> parseGroup(cs, score, count, depth)  # skip commas
      ["<" | cs ]   -> # parse garbage chunk
          { stream, garbageCount } = parseGarbage(cs, 0)
          parseGroup(stream, score, count + garbageCount, depth)
      ["{" | cs]    -> # parse child group
          {stream, groupScore, groupCount} = parseGroup(cs, 0, 0, depth + 1)
          parseGroup(stream, score + groupScore, count + groupCount, depth)
      ["}" | cs]    -> {cs, score + depth, count}  # end of group
    end
  end

  def parseGarbage(stream, count) do
    case stream do
      ["!", _ | cs] -> parseGarbage(cs, count)    # skip escaped chars
      [">" | cs]    -> {cs, count}                # end of garbage
      [ _ | cs ]    -> parseGarbage(cs, count+1)  # one moregarbage char
    end
  end

  def parseStream(stream, score, count) do
    case stream do
      ["!", _ | cs] -> parseStream(cs, score, count)  # skip escaped chars
      ["{" | cs]    -> # parse group
          {stream, groupScore, groupCount} = parseGroup(cs, 0, 0, 1)
          parseStream(stream, score + groupScore, count + groupCount)
      ["<" | cs]    -> # parse garbage
          {stream, garbageCount} = parseGarbage(cs, 0)
          parseStream(stream, score, count + garbageCount)
      []            -> {score, count} # end of data
      [c, _]       ->
          IO.puts(inspect stream)
          raise "Unexpected stream character \"#{c}\""
    end
  end

  def part1 do
    { score, _ } = loadFile(@filename)
      |> String.graphemes()
      |> parseStream(0, 0)
    score
  end

  def part2 do
    { _, count } = loadFile(@filename)
      |> String.graphemes()
      |> parseStream(0, 0)
    count
  end

end

IO.puts(Day9.part1())
IO.puts(Day9.part2())
