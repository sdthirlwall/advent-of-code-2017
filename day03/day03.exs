#!/usr/bn/env elixir

defmodule Day3 do

  def ring 0 do
    [ 0 ]
  end

  def ring 1 do
    [ 1, 2, 1, 2, 1, 2, 1, 2 ]
  end

  def ring n do
    first = n .. 2*n - 1
    second = Enum.map(first, &( &1 + 1 ))
    part = Enum.reverse(first) ++ second
    part ++ part ++ part ++ part
  end

  def part1 do
    input = 277678
    Stream.iterate(0, &(&1+1))
      |> Stream.flat_map(&ring/1)
      |> Stream.with_index(1)
      |> Enum.find(fn {_, n} -> n == input end)
      |> elem(0)
  end

  def addPos({ px, py }, { ox, oy }) do
    { px + ox, py + oy }
  end

  def sumNeighbours(board, pos) do
    offsets = for x <- -1..1, y <- -1..1, do: {x,y}
    Enum.map(offsets, fn off -> Map.get(board, addPos(pos, off), 0) end)
      |> Enum.sum
  end

  def turnLeft({ dx, dy }) do
    { -dy, dx }
  end

  def updateDir(board, pos, dir) do
    nextDir = turnLeft(dir)
    nextPos = addPos(pos, nextDir)
    if (Map.has_key?(board, nextPos)) do
      dir
    else
      nextDir
    end
  end

  def iterate(input, board, pos, dir) do
    pos = addPos(pos, dir)
    value = sumNeighbours(board, pos)
    board = Map.put(board, pos, value)
    if (value > input) do
      value
    else
      dir = updateDir(board, pos, dir)
      iterate(input, board, pos, dir)
    end
  end

  def part2 do
    input = 277678
    pos = { 0, 0 }
    dir = { 1, 0 }
    board = %{ pos => 1 }

    iterate(input, board, pos, dir)
  end

end

IO.puts(inspect Day3.part1)
IO.puts(inspect Day3.part2)
