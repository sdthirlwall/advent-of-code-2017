#!/usr/bin/env elixir

defmodule Day25 do

  defmodule TM do
    defstruct [
      :state,
      :cycles,
      tapeH: 0,
      tapeL: [ ],
      tapeR: [ ],
      transitions: %{ },
    ]

    def makeTransition(state, readSymbol, writeSymbol, move, nextState) do
      {
        {
          String.to_atom(state),
          String.to_integer(readSymbol),
        },
        {
          String.to_atom(nextState),
          String.to_integer(writeSymbol),
          String.to_atom(move),
        },
      }
    end

    def parse(defn) do
#      Begin in state A.
#      Perform a diagnostic checksum after 6 steps.
#
#      In state A:
#        If the current value is 0:
#          - Write the value 1.
#          - Move one slot to the right.
#          - Continue with state B.
#        If the current value is 1:
#          - Write the value 0.
#          - Move one slot to the left.
#          - Continue with state B.
#
#      In state B:
#        If the current value is 0:
#          - Write the value 1.
#          - Move one slot to the left.
#          - Continue with state A.
#        If the current value is 1:
#          - Write the value 1.
#          - Move one slot to the right.
#          - Continue with state A.

      header = ~r/Begin in state ([A-Z]+)\.\nPerform a diagnostic checksum after (\d+) steps\./m

      state = ~r/In state ([A-Z]+):.*?value is (\d+).*?value (\d+).*?to the (left|right).*?state ([A-Z]+).*?value is (\d+).*?value (\d+).*?to the (left|right).*?state ([A-Z]+)/ms

      [ _, start, diagAfter ] = Regex.run(header, defn)

      transitions =
        Regex.scan(state, defn)
          |> Enum.flat_map(fn [ _, state, match0, write0, move0, next0, match1, write1, move1, next1 ] ->
              [ makeTransition(state, match0, write0, move0, next0),
                makeTransition(state, match1, write1, move1, next1) ]
            end)
          |> Enum.into(%{ })

      %TM{
        state:        String.to_atom(start),
        cycles:       String.to_integer(diagAfter),
        transitions:  transitions,
      }
    end

    def checksum(tm) do
      tm.tapeH + Enum.sum(tm.tapeL) + Enum.sum(tm.tapeR)
    end

    def popTape([]), do: { 0, [ ] }
    def popTape([ x | xs ]), do: { x, xs }

    def shift(tm, :left) do
      # move head left pushes tapeH onto tapeR, and pops tapeL onto tapeH
      { tapeH, tapeL } = popTape(tm.tapeL)
      %{ tm | tapeH: tapeH, tapeL: tapeL, tapeR: [ tm.tapeH | tm.tapeR ] }
    end

    def shift(tm, :right) do
      # move head right pushes tapeH onto tapeL, and pops tapeR onto tapeH
      { tapeH, tapeR } = popTape(tm.tapeR)
      %{ tm | tapeH: tapeH, tapeL: [ tm.tapeH | tm.tapeL ], tapeR: tapeR }
    end

    def move(tm, { state, symbol, dir }) do
      %{ tm | state: state, tapeH: symbol, cycles: tm.cycles - 1 }
        |> shift(dir)
    end

    def step(tm) do
      move(tm, tm.transitions[{ tm.state, tm.tapeH }])
    end

    def run(tm) do
      if tm.cycles === 0 do
        checksum(tm)
      else
        step(tm) |> run
      end
    end
  end

  def parseFile(filename) do
    File.open!(filename, [:read], fn file ->
      IO.read(file, :all)
    end) |> TM.parse
  end

  def part1(filename) do
    parseFile(filename)
      |> TM.run
  end

end

#Day25.part1("test.txt") |> IO.puts
Day25.part1("input.txt") |> IO.puts
