#!/usr/bin/env elixir

defmodule Day18 do

  defmodule Machine do
    defstruct inst: [ ], reg: %{ }, ip: 0, freq: nil, running: true
  end

  def fetch(machine, arg) do
    case Integer.parse(arg) do
      { imm, "" } -> imm
      :error -> Map.get(machine.reg, arg, 0)
    end
  end

  def store(machine, reg, value) do
    %{machine | reg: Map.put(machine.reg, reg, value) }
  end

  def parseInst(i = [ "add", dest, src ]) do
    fn machine ->
      #IO.inspect(i)
      store(machine, dest, fetch(machine, dest) + fetch(machine, src))
    end
  end

  def parseInst(i = [ "jgz", src, offset ]) do
    fn machine ->
      #IO.inspect(i)
      if fetch(machine, src) > 0 do
        %{machine | ip: machine.ip + fetch(machine, offset) - 1}
      else
        machine
      end
    end
  end

  def parseInst(i = [ "mod", dest, src ]) do
    fn machine ->
      #IO.inspect(i)
      store(machine, dest, rem(fetch(machine, dest), fetch(machine, src)))
    end
  end

  def parseInst(i = [ "mul", dest, src ]) do
    fn machine ->
      #IO.inspect(i)
      store(machine, dest, fetch(machine, dest) * fetch(machine, src))
    end
  end

  def parseInst(i = [ "rcv", dest ]) do
    fn machine ->
      #IO.inspect(i)
      if fetch(machine, dest) != 0 do
        IO.puts(machine.freq)
        %{machine | running: false }
      else
        machine
      end
    end
  end

  def parseInst(i = [ "set", dest, src ]) do
    fn machine ->
      #IO.inspect(i)
      store(machine, dest, fetch(machine, src))
    end
  end

  def parseInst(i = [ "snd", src ]) do
    fn machine ->
      #IO.inspect(i)
      %{machine | freq: fetch(machine, src) }
    end
  end

  def parseInst(i) do
    fn machine ->
      #IO.inspect(i)
      %{machine | running: false }
    end
  end

  def makeMachine(inst) do
    %Machine{ inst: inst }
  end

  def step(machine) do
    case Enum.at(machine.inst, machine.ip) do
      nil  -> %{machine | running: false}
      inst -> inst.(%{machine | ip: machine.ip + 1 })
    end
  end

  def run(machine) do
    #IO.inspect(machine)
    if machine.running do
      step(machine) |> run
    else
      machine
    end
  end

  def parseLine(line) do
    String.trim(line)
      |> String.split(~r/\s+/)
      |> parseInst
  end

  def parseFile(filename) do
    File.stream!(filename)
      |> Stream.map(&parseLine/1)
      |> Enum.to_list
      |> makeMachine
  end

  def part1 do
    parseFile("input.txt")
      |> run
      |> IO.inspect
    :ok
  end

end

IO.puts(Day18.part1())
