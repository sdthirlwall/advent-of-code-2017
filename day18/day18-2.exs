#!/usr/bin/env elixir

defmodule Day18 do

  defmodule Machine do
    defstruct inst: [ ], reg: %{ }, ip: 0, sends: 0, parent: nil, sibling: nil, id: nil, running: true
  end

  def makeFetch(arg) do
    case Integer.parse(arg) do
      { imm, "" } -> fn _ -> imm end
      :error -> fn machine -> Map.get(machine.reg, arg, 0) end
    end
  end

  def store(machine, reg, value) do
    %{machine | reg: Map.put(machine.reg, reg, value) }
  end

  def binOp(op, dest, src) do
    fetchDest = makeFetch(dest)
    fetchSrc = makeFetch(src)
    fn machine ->
      store(machine, dest, op.(fetchDest.(machine), fetchSrc.(machine)))
    end
  end

  def parseInst([ "add", dest, src ]) do
    binOp(&Kernel.+/2, dest, src)
  end

  def parseInst([ "jgz", src, offset ]) do
    fetchSrc = makeFetch(src)
    fetchOffset = makeFetch(offset)
    fn machine ->
      if fetchSrc.(machine) > 0 do
        %{machine | ip: machine.ip + fetchOffset.(machine) - 1}
      else
        machine
      end
    end
  end

  def parseInst([ "mod", dest, src ]) do
    binOp(&Kernel.rem/2, dest, src)
  end

  def parseInst([ "mul", dest, src ]) do
    binOp(&Kernel.*/2, dest, src)
  end

  def parseInst([ "rcv", dest ]) do
    fn machine ->
      receive do
        value -> store(machine, dest, value)
      after
        100 -> %{ machine | running: false }
      end
    end
  end

  def parseInst([ "set", dest, src ]) do
    fetchSrc = makeFetch(src)
    fn machine ->
      store(machine, dest, fetchSrc.(machine))
    end
  end

  def parseInst([ "snd", src ]) do
    fetchSrc = makeFetch(src)
    fn machine ->
      send(machine.sibling, fetchSrc.(machine))
      %{machine | sends: machine.sends + 1}
    end
  end

  def parseInst(_) do
    fn machine ->
      %{machine | running: false }
    end
  end

  def makeMachine(inst, parent, id) do
    %Machine{ inst: inst, id: id, parent: parent, reg: %{ "p" => id } }
  end

  def startMachine(machine) do
    receive do
      sibling -> run(%{machine | sibling: sibling })
    end
  end

  def step(machine) do
    case Enum.at(machine.inst, machine.ip) do
      nil  -> %{machine | running: false}
      inst -> inst.(%{machine | ip: machine.ip + 1 })
    end
  end

  def run(machine) do
    if machine.running do
      step(machine) |> run
    else
      send(machine.parent, machine)
    end
  end

  def parseLine(line) do
    String.trim(line)
      |> String.split(~r/\s+/)
      |> parseInst
  end

  def parseFile(filename) do
    File.stream!(filename)
      |> Stream.map(&parseLine/1)
      |> Enum.to_list
  end

  def wait do
    receive do
      %Machine{ id: 1, sends: n } -> n
    end
  end

  def part2 do
    inst = parseFile("input.txt")
    parent = self()

    pid0 = spawn(fn -> makeMachine(inst, parent, 0) |> startMachine end)
    pid1 = spawn(fn -> makeMachine(inst, parent, 1) |> startMachine end)
    send(pid0, pid1)
    send(pid1, pid0)

    wait()
  end

end

IO.puts(Day18.part2())
