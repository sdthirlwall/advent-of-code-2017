#!/usr/bin/env elixir

defmodule Day8 do

  @filename "input.txt"

  def parseLine(line) do
    [ tgt, op, arg, _, src, cmp, val ] = String.trim(line) |> String.split(" ")
    %{
      :target       => tgt,
      :operator     => String.to_atom(op),
      :operand      => String.to_integer(arg),

      :source       => src,
      :comparison   => String.to_atom(cmp),
      :comparand    => String.to_integer(val),

      :line         => String.trim(line),
    }
  end

  def parseFile(filename) do
    File.stream!(filename)
      |> Stream.map(&parseLine/1)
  end

  def fetch(registers, register) do
    Map.get(registers, register, 0)
  end

  def store(registers, register, value) do
    Map.put(registers, register, value)
  end

  def test(registers, instruction) do
    src = fetch(registers, instruction.source)
    cmp = instruction.comparand
    case instruction.comparison do
      :== -> src == cmp
      :!= -> src != cmp
      :>  -> src >  cmp
      :>= -> src >= cmp
      :<  -> src <  cmp
      :<= -> src <= cmp
    end
  end

  def execute(registers, instruction) do
    value = fetch(registers, instruction.target)
    offset = case instruction.operator do
      :inc -> +instruction.operand
      :dec -> -instruction.operand
    end
    Map.put(registers, instruction.target, value + offset)
  end

  def doInstruction(instruction, registers) do
    if (test(registers, instruction)) do
      execute(registers, instruction)
    else
      registers
    end
  end

  def maxRegisterValue(registers) do
    Map.values(registers)
      |> Enum.max
  end

  def part1 do
    parseFile(@filename)
      |> Enum.reduce(%{}, &doInstruction/2)
      |> maxRegisterValue
  end

  def part2 do
    parseFile(@filename)
      |> Stream.scan(%{}, &doInstruction/2)
      |> Enum.map(&maxRegisterValue/1)
      |> Enum.max
  end
end

IO.puts(Day8.part1)
IO.puts(Day8.part2)
