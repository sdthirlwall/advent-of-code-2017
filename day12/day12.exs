#!/usr/bin/env elixir

defmodule Day12 do
  @filename "input.txt"

  def parseLine(line) do
    [ from, to ] = String.trim(line) |> String.split(" <-> ")
    { String.to_integer(from),
      String.split(to, ", ") |> Enum.map(&String.to_integer/1) }
  end

  def parseFile(filename) do
    File.stream!(filename)
      |> Stream.map(&parseLine/1)
      |> Enum.to_list
  end

  def makeConnectionTable(connections) do
    Enum.reduce(connections, %{ }, fn { from, to }, table ->
      Map.put(table, from, to)
    end)
  end

  def addNeighbours(connectionTable, group, from) do
    if MapSet.member?(group, from) do
      group
    else
      Map.get(connectionTable, from)
        |> Enum.reduce(MapSet.put(group, from),
                       fn(to, s) -> addNeighbours(connectionTable, s, to) end)
    end
  end

  def makeGroup(connectionTable, start) do
    addNeighbours(connectionTable, MapSet.new, start)
  end

  def removeNeighbours(from, connectionTable) do
    { neighbours, table } = Map.pop(connectionTable, from, [ ])
    Enum.reduce(neighbours, table, &removeNeighbours/2)
  end

  def removeGroup(connectionTable) do
    start = Map.keys(connectionTable) |> List.first
    removeNeighbours(start, connectionTable)
  end

  def countGroups(connectionTable) do
    Stream.iterate(connectionTable, &removeGroup/1)
      |> Stream.take_while(fn ct -> !Enum.empty?(ct) end)
      |> Enum.count
  end

  def part1 do
    parseFile(@filename)
      |> makeConnectionTable
      |> makeGroup(0)
      |> MapSet.size
  end

  def part2 do
    parseFile(@filename)
      |> makeConnectionTable
      |> countGroups
  end

end

IO.puts(Day12.part1())
IO.puts(Day12.part2())
