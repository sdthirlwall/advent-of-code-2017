#!/usr/bin/env elixir

defmodule Day10 do

  @filename "input.txt"

  import Bitwise

  # { items, curPos, skipSize }

  def parseFile1(filename) do
    {:ok, data} = File.open(filename, [:read], fn file ->
      IO.read(file, :all)
    end)
    String.trim(data)
      |> String.split(",")
      |> Enum.map(&String.to_integer/1)
  end

  def parseFile2(filename) do
    {:ok, data} = File.open(filename, [:read], fn file ->
      IO.read(file, :all)
    end)
    result = String.trim(data) |> String.to_charlist
    result ++ [ 17, 31, 73, 47, 23 ]
  end

  # rotateLeft([ 1, 2, 3, 4, 5 ], 2) = [ 3, 4, 5, 1, 2 ]
  def rotateLeft(list, n) do
    { lhs, rhs }  = Enum.split(list, n)
    rhs ++ lhs
  end

  # reverseFront([ 1, 2, 3, 4, 5 ], 2) = [ 2, 1, 3, 4, 5 ]
  def reverseFront(list, n) do
    { lhs, rhs }  = Enum.split(list, n)
    Enum.reverse(lhs) ++ rhs
  end

  # Don't try reversing around the end of the list or any such thing.
  # Rotate the list so the part to be reversed is at the start, reverse it,
  # and then rotate it back into place.
  def rotateList(list, curPos, rotation) do
    list
      |> rotateLeft(curPos)
      |> reverseFront(rotation)
      |> rotateLeft(-curPos)
  end

  def step(rotation, { items, curPos, skipSize }) do
    {
      rotateList(items, curPos, rotation),
      rem(curPos + rotation + skipSize, length(items)),
      skipSize + 1
    }
  end

  def make(size) do
    { Enum.to_list(0 .. size-1), 0, 0 }
  end

  def part1 do
    [ first, second | _rest ] =
      parseFile1(@filename)
        |> Enum.reduce(make(256), &step/2)
        |> elem(0)
    first * second
  end

  def iterate(n, acc, f) do
    Stream.iterate(acc, f)
      |> Stream.take(n + 1)
      |> Enum.reverse()
      |> hd
  end

  def doPass(input, hash) do
    Enum.reduce(input, hash, &step/2)
  end

  def xorList(list) do
    Enum.reduce(list, 0, &bxor/2)
  end

  def intToHexStr(i) do
    Integer.to_string(i, 16)
      |> String.pad_leading(2, "0")
  end

  def part2 do
    input = parseFile2(@filename)
    iterate(64, make(256), &(doPass(input, &1)))
      |> elem(0)                  # pluck out just the list
      |> Enum.chunk(16)           # break into chunks of 16
      |> Enum.map(&xorList/1)     # xor each chunk to a single int
      |> Enum.map(&intToHexStr/1) # convert to %02X
      |> Enum.join("")
      |> String.downcase()
  end
end

defmodule Day14 do

  @input "amgozmfv";

  def intToBin(i) do
    Integer.to_string(i, 2)
      |> String.pad_leading(8, "0")
      |> String.graphemes
      |> Enum.map(&String.to_integer/1)
  end

  def knothash(input) do
    Day10.iterate(64, Day10.make(256), &Day10.doPass(input, &1))
      |> elem(0)                      # pluck out just the list
      |> Enum.chunk(16)               # break into chunks of 16
      |> Enum.map(&Day10.xorList/1)   # xor each chunk to a single int
      |> Enum.flat_map(&Day14.intToBin/1)  # convert to binary digits
  end

  @salt [ 17, 31, 73, 47, 23 ]
  def makeSeq(input, n) do
    String.to_charlist(input <> "-" <> Integer.to_string(n)) ++ @salt
  end

  def makeGrid(input) do
    Enum.to_list(0..127)
      |> Enum.map(&(makeSeq(input, &1) |> knothash))
  end

  def makeHashGrid(input) do
    cellList =
      for {row, y} <- Enum.with_index(makeGrid(input)),
          {cell, x} <- Enum.with_index(row),
          cell == 1,
        do: {x, y}
    Enum.reduce(cellList, %{}, fn(coord, grid) -> Map.put(grid, coord, 1) end)
  end

  def part1 do
    makeGrid(@input)
      |> List.flatten
      |> Enum.count(&(&1 == 1))
  end

  def removeRegion(grid, cell = {x,y}) do
    if Map.has_key?(grid, cell) do
      Map.drop(grid, [cell])
        |> removeRegion({ x + 1, y })
        |> removeRegion({ x - 1, y })
        |> removeRegion({ x, y + 1 })
        |> removeRegion({ x, y - 1 })
    else
      grid
    end
  end

  def countRegions(grid, count) do
    case Map.keys(grid) do
      [ ]           -> count
      [ start | _ ] -> removeRegion(grid, start) |> countRegions(count + 1)
    end
  end

  def part2 do
    makeHashGrid(@input)
      |> countRegions(0)
  end

end

IO.puts(Day14.part1())
IO.puts(Day14.part2())
