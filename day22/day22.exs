#!/usr/bin/env elixir

defmodule Day22 do

  defmodule Grid do
    defstruct [
      infected: MapSet.new,
      pos: { 0, 0 },
      dir: { 0, -1 },
      infections: 0,
    ]

    # x is right, y is down

    def turnLeft({ x, y }) do
      # 1,2 -> 2,-1
      { y, -x }
    end

    def turnRight({ x, y }) do
      # 1,2 -> -2,1
      { -y, x }
    end

    def move({ px, py }, { dx, dy }) do
      { px + dx, py + dy }
    end

    def parse(lines) do
      infected = (for { line, y } <- Enum.with_index(lines),
                      { char, x } <- String.graphemes(line) |> Enum.with_index,
                      char == "#",
                     do: {x, y}) |> MapSet.new

      height = Enum.count(lines)
      width  = Enum.to_list(lines) |> List.first |> String.length

      %Grid{ infected: infected, pos: { div(height, 2), div(width, 2)  } }
    end

    def burst(grid) do
      if MapSet.member?(grid.infected, grid.pos) do
        # infected -> clean, turn right
        dir = turnRight(grid.dir)
        pos = move(grid.pos, dir)
        %{grid | pos: pos, dir: dir,
                 infected: MapSet.delete(grid.infected, grid.pos) }
      else
        # clean -> infected, turn left
        dir = turnLeft(grid.dir)
        pos = move(grid.pos, dir)
        %{grid | pos: pos, dir: dir, infections: grid.infections + 1,
                 infected: MapSet.put(grid.infected, grid.pos) }
      end
    end

    def run(grid) do
      Stream.iterate(grid, &burst/1)
    end

  end

  def parseFile(filename) do
    File.stream!(filename)
      |> Stream.map(&String.trim/1)
      |> Grid.parse
      |> IO.inspect
  end

  def part1 do
    #{ filename, iterations } = { "test.txt", 70 }
    #{ filename, iterations } = { "test.txt", 10_000 }
    { filename, iterations } = { "input.txt", 10_000 }
    parseFile(filename)
      |> Grid.run
      |> Stream.take(iterations + 1)
      |> Enum.take(-1)
      |> IO.inspect
  end
end

Day22.part1
