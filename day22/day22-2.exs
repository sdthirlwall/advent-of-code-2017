#!/usr/bin/env elixir

defmodule Day22 do

  defmodule Grid do
    defstruct [
      cells: %{ },
      pos: { 0, 0 },
      dir: { 0, -1 },
      infections: 0,
    ]

    # x is right, y is down
    def turn({x, y}, state) do
      case state do
        :clean    -> {  y, -x } # left
        :weakened -> {  x,  y } # straight on
        :infected -> { -y,  x } # right
        :flagged  -> { -x, -y } # reverse
      end
    end

    def nextState(state) do
      case state do
        :clean    -> :weakened
        :weakened -> :infected
        :infected -> :flagged
        :flagged  -> :clean
      end
    end

    def move({ px, py }, { dx, dy }) do
      { px + dx, py + dy }
    end

    def parse(lines) do
      infected =
        for { line, y } <- Enum.with_index(lines),
            { char, x } <- String.graphemes(line) |> Enum.with_index,
            char == "#",
            do: {x, y}

      cells = Enum.reduce(infected, %{ }, fn ({x, y}, map) ->
        Map.put(map, {x, y}, :infected) end)

      height = Enum.count(lines)
      width  = Enum.to_list(lines) |> List.first |> String.length

      %Grid{ cells: cells, pos: { div(height, 2), div(width, 2)  } }
    end

    def burst(grid) do
      state = Map.get(grid.cells, grid.pos, :clean)
      dir = turn(grid.dir, state)
      pos = move(grid.pos, dir)
      next = nextState(state)
      incr = if next == :infected do 1 else 0 end
      %{grid | pos: pos, dir: dir,
               cells: Map.put(grid.cells, grid.pos, next),
               infections: grid.infections + incr}
    end

    def run(grid) do
      Stream.iterate(grid, &burst/1)
    end


  end

  def parseFile(filename) do
    File.stream!(filename)
      |> Stream.map(&String.trim/1)
      |> Grid.parse
      |> IO.inspect
  end

  def part2 do
    #{ filename, iterations } = { "test.txt", 100 }
    #{ filename, iterations } = { "test.txt", 10_000_000 }
    { filename, iterations } = { "input.txt", 10_000_000 }
    parseFile(filename)
      |> Grid.run
      |> Stream.take(iterations + 1)
      |> Enum.take(-1)
      |> IO.inspect
  end
end

Day22.part2
