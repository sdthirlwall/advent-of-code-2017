#!/usr/bin/env elixir

defmodule Day6 do

  @filename "input.txt"

  def parseLine(line) do
    String.trim_trailing(line)
      |> String.split("\t")
      |> Enum.map(&String.to_integer/1)
  end

  def parseFile(filename) do
    File.stream!(filename)
      |> Enum.map(&parseLine/1)
      |> List.first
  end

  def index_of(list, item) do
    Enum.find_index(list, &(&1 === item))
  end

  def distribute(blocks, 0, _index), do: blocks

  def distribute(blocks, count, index) do
    blocks = List.update_at(blocks, index, &(&1 + 1))
    distribute(blocks, count-1, rem(index + 1, length(blocks)))
  end

  def cycle(blocks) do
    maxValue = Enum.max(blocks)
    maxIndex = index_of(blocks, maxValue)
    blocks = List.update_at(blocks, maxIndex, fn _ -> 0 end)
    distribute(blocks, maxValue, rem(maxIndex + 1, length(blocks)))
  end

  def uniquePrefix(stream) do
    Stream.transform(stream, MapSet.new, fn item, seen ->
      if MapSet.member?(seen, item) do
        {:halt, seen}  # end the stream as soon as we find a duplicate
      else
        {[ item ], MapSet.put(seen, item)}
      end
    end)
  end

  def part1 do
    parseFile(@filename)
      |> Stream.iterate(&cycle/1)
      |> uniquePrefix
      |> Enum.count
  end

  def part2 do
    configs = parseFile(@filename)
      |> Stream.iterate(&cycle/1)
      |> uniquePrefix
      |> Enum.to_list

    # List of configs does not contain the repeated state, so run cycle one more
    # time to get it back.
    repeatedConfig = cycle(List.last(configs))
    startIndex = index_of(configs, repeatedConfig)
    length(configs) - startIndex
  end
end

IO.puts(Day6.part1)
IO.puts(Day6.part2)
