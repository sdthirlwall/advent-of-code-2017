#!/usr/bin/env elixir

defmodule Day19 do

  @north { 0, -1 }
  @south { 0, +1 }
  @east  { +1, 0 }
  @west  { -1, 0 }

  defmodule Diagram do
    defstruct [
      grid: %{ },
      pos:  { 0, 0 },
      dir:  { 0, 1 },
      path: [ ],
      steps: 0,
    ]
  end

  def readLines(filename) do
    File.stream!(filename)
      |> Stream.with_index
  end

  def readChars(line) do
    String.trim_trailing(line)
      |> String.graphemes
      |> Enum.with_index
  end

  def parseFile(filename) do
    for { line, y } <- readLines(filename),
        { char, x } <- readChars(line),
        char != " ",
      do: { { x, y }, char }
  end

  def buildDiagram(cells) do
    [ { pos, _char } | _rest ] = cells
    grid = Enum.reduce(cells, %{ }, fn({ pos, char}, map) ->
                Map.put(map, pos, char) end)

    %Diagram{ pos: pos, grid: grid }
  end

  def addPos({ px, py }, { dx, dy }) do
    { px + dx, py + dy }
  end

  def move(diagram) do
    grid = Map.drop(diagram.grid, [ diagram.pos ])
    pos  = addPos(diagram.pos, diagram.dir)
    %{ diagram | grid: grid, pos: pos, steps: diagram.steps + 1 }
  end

  def nextDir(diagram) do
    dirs = [ @north, @west, @south, @east ]
    grid = diagram.grid
    pos = diagram.pos
    dir = Enum.find(dirs, fn dir -> Map.has_key?(grid, addPos(pos, dir)) end)
    %{ diagram | dir: dir }
  end

  def step(diagram) do
    char = Map.get(diagram.grid, diagram.pos, " ")
    newDiagram = cond do
      char === "+"        -> nextDir(diagram)
      char =~ ~r/^[A-Z]$/ -> %{ diagram | path: [ char | diagram.path ] }
      true                -> diagram
    end
    move(newDiagram)
  end

  def run(diagram) do
    if (Map.size(diagram.grid) === 0) do
      diagram
    else
      step(diagram) |> run
    end
  end

  def part1 do
    parseFile("input.txt")
      |> buildDiagram
      |> run
      |> Map.get(:path)
      |> Enum.join
  end

  def part2 do
    parseFile("input.txt")
      |> buildDiagram
      |> run
      |> Map.get(:steps)
  end

end

IO.puts(Day19.part1)
IO.puts(Day19.part2)
