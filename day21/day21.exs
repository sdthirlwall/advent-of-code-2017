#!/usr/bin/env elixir

defmodule Day21 do

  defmodule Grid do
    # Cells is a set of {x,y} coords of the lit pixels
    defstruct [ :size, cells: MapSet.new ]

    def parse(desc) do
      [ firstRow | _ ] = String.split(desc, "/")
      cells =
        for {row, y} <- String.split(desc, "/") |> Enum.with_index,
            {char, x} <- String.graphemes(row) |> Enum.with_index,
            char === "#",
            do: {x, y}

      %Grid{ size: String.length(firstRow), cells: MapSet.new(cells) }
    end

    def getSubGrid(grid, size, sx, sy) do
      xOffset = sx * size
      yOffset = sy * size
      cells =
        for y <- 0..(size-1),
            x <- 0..(size-1),
            MapSet.member?(grid.cells, { x + xOffset, y + yOffset }),
            do: { x, y }
      %Grid{ size: size, cells: MapSet.new(cells) }
    end

    def putSubGrid(grid, subGrid, sx, sy) do
      xOffset = sx * subGrid.size
      yOffset = sy * subGrid.size
      cells =
        Enum.map(subGrid.cells, fn { x, y } -> { x + xOffset, y + yOffset } end)
          |> MapSet.new
          |> MapSet.union(grid.cells)
      %{grid | cells: cells}
    end

    def transform(grid, f) do
      %{grid | cells: Enum.map(grid.cells, f) |> MapSet.new}
    end

    def rotate(grid) do
      m = grid.size - 1
      transform(grid, fn {x, y} -> {m - y, x} end)
    end

    def flip(grid) do
      m = grid.size - 1
      transform(grid, fn {x, y} -> {x, m - y} end)
    end

    def variations(grid) do
      Stream.iterate(grid, &rotate/1)
        |> Enum.take(4)
        |> Enum.flat_map(fn grid -> [ grid, flip(grid) ] end)
    end

    defimpl Inspect, for: Grid do
      def inspectCell(grid, x, y) do
        if MapSet.member?(grid.cells, { x, y }) do
          "#"
        else
          "."
        end
      end

      def inspectRow(grid, y) do
        Enum.map(0..(grid.size-1), fn x -> inspectCell(grid, x, y) end)
          |> Enum.join("")
      end

      def inspect(grid, _opts) do
        "\n" <> (Enum.map(0..(grid.size-1), fn y -> inspectRow(grid, y) end)
          |> Enum.join("\n")) <> "\n"
      end
    end
  end

  def parseFile(filename) do
    File.stream!(filename)
      |> Stream.map(&parseLine/1)
  end

  def parseLine(line) do
    [ from, to ] =
      String.trim(line)
        |> String.split(" => ")
        |> Enum.map(&Grid.parse/1)
    { from, to }
  end

  def addMapping(map, from, to) do
    Grid.variations(from)
      |> Enum.reduce(map, &(Map.put(&2, &1, to)))
  end

  def transform2x2s(map, grid) do
    subGridCount = div(grid.size, 2)
    subGrids =
      for y <- 0..subGridCount-1,
          x <- 0..subGridCount-1,
          src = Grid.getSubGrid(grid, 2, x, y),
          do: { x, y, map[src] }

    Enum.reduce(subGrids, %Grid{ size: subGridCount * 3 },
      fn({ x, y, subGrid }, acc) -> Grid.putSubGrid(acc, subGrid, x, y) end)
  end

  def transform3x3s(map, grid) do
    subGridCount = div(grid.size, 3)
    subGrids =
      for y <- 0..subGridCount-1,
          x <- 0..subGridCount-1,
          src = Grid.getSubGrid(grid, 3, x, y),
          do: { x, y, map[src] }

    Enum.reduce(subGrids, %Grid{ size: subGridCount * 4 },
      fn({ x, y, subGrid }, acc) -> Grid.putSubGrid(acc, subGrid, x, y) end)
  end

  def transformGrid(grid, map, size) do
    subGridCount = div(grid.size, size)
    subGrids =
      for y <- 0..subGridCount-1,
          x <- 0..subGridCount-1,
          src = Grid.getSubGrid(grid, size, x, y),
          do: { x, y, map[src] }

    Enum.reduce(subGrids, %Grid{ size: subGridCount * (size + 1) },
      fn({ x, y, subGrid }, acc) -> Grid.putSubGrid(acc, subGrid, x, y) end)
  end

  def transformGrid(grid, map) do
    if rem(grid.size, 2) == 0 do
      transformGrid(grid, map, 2)
    else
      transformGrid(grid, map, 3)
    end
  end

  def part1 do
    start = Grid.parse(".#./..#/###")
    map =
      parseFile("input.txt")
        |> Enum.reduce(%{ }, fn({from, to}, map) -> addMapping(map, from, to) end)

    Stream.iterate(start, &(transformGrid(&1, map)))
#      |> Stream.map(&IO.inspect/1)
      |> Enum.take(5+1)
      |> List.last
      |> Map.get(:cells)
      |> MapSet.size

  end

  def part2 do
    start = Grid.parse(".#./..#/###")
    map =
      parseFile("input.txt")
        |> Enum.reduce(%{ }, fn({from, to}, map) -> addMapping(map, from, to) end)

    Stream.iterate(start, &(transformGrid(&1, map)))
#      |> Stream.map(fn grid -> IO.puts(grid.size) ; grid end)
      |> Enum.take(18+1)
      |> List.last
      |> Map.get(:cells)
      |> MapSet.size

  end


end

Day21.part1 |> IO.puts
Day21.part2 |> IO.puts
