#!/usr/bin/env elixir

defmodule Day4 do

  @filename "input.txt"

  def parseLine(line) do
    String.split(line, " ")
  end

  def parseFile(filename) do
    File.stream!(filename)
      |> Stream.map(&String.trim_trailing/1)
      |> Stream.map(&parseLine/1)
  end

  def isUniq(list) do
    Enum.uniq(list) == list
  end

  def part1 do
    parseFile(@filename)
      |> Enum.filter(&isUniq/1)
      |> length
  end

  def sortChars(string) do
    String.graphemes(string)
      |> Enum.sort()
      |> Enum.join("")
  end

  def sortWords(list) do
    Enum.map(list, &sortChars/1)
  end

  def part2 do
    parseFile(@filename)
      |> Stream.map(&sortWords/1)
      |> Enum.filter(&isUniq/1)
      |> length
  end
end

IO.puts(Day4.part1)
IO.puts(Day4.part2)
