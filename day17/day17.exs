#!/usr/bin/env elixir

defmodule Day17 do

  @step   382
  @limit  50_000_000

  def makeMove(step) do
    fn { buffer, pos, move } ->
      newPos = rem(pos + step, move) + 1
      { List.insert_at(buffer, newPos, move), newPos, move + 1 }
    end
  end

  def part1 do
    buffer = Stream.iterate({ [ 0 ], 0, 1 }, makeMove(@step))
      |> Stream.take(2018)
      |> Enum.take(-1)
      |> List.first
      |> elem(0)
    index = Enum.find_index(buffer, &(&1 == 2017))
    Enum.at(buffer, index+1)
  end

  def nextPos(move, pos \\ 1), do: rem(pos + @step, move) + 1

  def iterate(state) do
    case state do
      { first,   _, @limit } -> first
      { _,       1, move }   -> iterate({ move - 1, nextPos(move), move + 1 })
      { first, pos, move }   -> iterate({ first, nextPos(move, pos), move + 1 })
    end
  end

  def part2 do
    iterate({ 0, 0, 1 })
  end

end

IO.puts(Day17.part1())
IO.puts(Day17.part2())
