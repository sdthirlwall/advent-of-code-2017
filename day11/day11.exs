#!/usr/bin/env elixir

defmodule Day11 do

  @filename "input.txt"

  def parseFile(filename) do
    File.stream!(filename)
      |> Stream.map(&String.trim/1)
      |> Stream.flat_map(fn line -> String.split(line, ",") end)
      |> Stream.map(&String.to_atom/1)
  end

  def doMove(:n,  { x, y }),  do: { x,     y + 2 }
  def doMove(:ne, { x, y }),  do: { x + 2, y + 1 }
  def doMove(:nw, { x, y }),  do: { x - 2, y + 1 }
  def doMove(:s,  { x, y }),  do: { x,     y - 2 }
  def doMove(:se, { x, y }),  do: { x + 2, y - 1 }
  def doMove(:sw, { x, y }),  do: { x - 2, y - 1 }

  def hexDistance({ x, y }) do
    # Just use the positive quadrant - the distances are the same.
    ax = abs x
    ay = abs y

    if ay * 2 > ax do
      div(ay + ax, 2)
    else
      div(ax, 2)
    end
  end

  def part1 do
    parseFile(@filename)
      |> Enum.reduce({ 0, 0 }, &doMove/2)
      |> hexDistance
  end

  def part2 do
    parseFile(@filename)
      |> Stream.scan({ 0, 0 }, &doMove/2)
      |> Stream.map(&hexDistance/1)
      |> Enum.max
  end
end

IO.inspect(Day11.part1)
IO.inspect(Day11.part2)
